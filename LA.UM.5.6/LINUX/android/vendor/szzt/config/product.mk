PRODUCT_PACKAGES += \
	szzt_vendor_keys\
	szzt_vendor_keys_zip\
	tcsdkimpl

PRODUCT_COPY_FILES += \
        vendor/szzt/etc/pppoe:system/bin/pppoe\
        vendor/szzt/etc/pppoe_connect.sh:system/bin/pppoe_connect.sh\
        vendor/szzt/etc/pppoe_disconnect.sh:system/bin/pppoe_disconnect.sh\
        vendor/szzt/etc/pppoe_connect_bin:system/bin/pppoe_connect_bin\
        vendor/szzt/etc/pppoe_disconnect_bin:system/bin/pppoe_disconnect_bin\
        vendor/szzt/etc/pppoe-repay:system/bin/pppoe-repay\
        vendor/szzt/etc/pppoe-sniff:system/bin/pppoe-sniff

PRODUCT_COPY_FILES += \
        vendor/szzt/etc/Effect_Tick.ogg:/system/media/audio/ui/Effect_Tick.ogg

PRODUCT_COPY_FILES += \
        vendor/szzt/etc/ztconfig_ethernet.sh:system/bin/ztconfig_ethernet.sh

PRODUCT_COPY_FILES += \
        vendor/szzt/etc/driver.tar:system/etc/driver.tar

PRODUCT_PACKAGES += \
    ZtLogApp\
	ZtTiSetting \
    ZtTiCaiLocationService \
	ZtTiCaiSystem\
	TCService\
	busybox\
	SzztPinyinIme

PRODUCT_BOOT_JARS += tcsdkimpl

PRODUCT_COPY_FILES += vendor/szzt/etc/bootanimation.zip:system/media/bootanimation.zip \
        vendor/szzt/etc/shutdownanimation.zip:system/media/shutdownanimation.zip

PRODUCT_COPY_FILES += vendor/szzt/etc/ticaiinit.sh:system/bin/ticaiinit.sh
PRODUCT_COPY_FILES += vendor/szzt/etc/devices.lst:system/cslc/config/devices.lst

#for tscsclient.tar
PRODUCT_COPY_FILES += vendor/szzt/etc/tscsclient.tar:system/etc/tscsclient.tar

#for easypos.tar
 PRODUCT_COPY_FILES += vendor/szzt/etc/easypos.tar:system/etc/easypos.tar


PRODUCT_COPY_FILES += \
        vendor/szzt/etc/zt_persist_keys:system/etc/zt_persist_keys

PRODUCT_PACKAGES += com.cslc.easypos
PRODUCT_PACKAGES += com.cslc.infodialog
PRODUCT_PACKAGES += com.cslc.ostools
PRODUCT_PACKAGES += com.cslc.screensaver
PRODUCT_PACKAGES += com.cslc.update
PRODUCT_PACKAGES += com.cslc.commonsign
