LOCAL_PATH:= $(call my-dir)

#$(shell mkdir -p $(OUT)/system/etc/)
#$(shell cp ~/easypos_source/easypos.tar $(OUT)/system/etc/)
#$(shell cp ~/tscsclient_source/tscsclient.tar $(OUT)/system/etc/)

include $(CLEAR_VARS)
LOCAL_MODULE       := szzt_vendor_keys
LOCAL_MODULE_TAGS  := optional
LOCAL_MODULE_CLASS := ETC
LOCAL_SRC_FILES    := $(LOCAL_MODULE)
LOCAL_MODULE_PATH := $(PRODUCT_OUT)/szzt/ota/key/
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE       := szzt_vendor_keys_zip
LOCAL_MODULE_TAGS  := optional
LOCAL_MODULE_CLASS := ETC
LOCAL_SRC_FILES    := szzt_vendor.x509.zip
LOCAL_MODULE_PATH := $(PRODUCT_OUT)/szzt/ota/key/
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE               := tcsdkimpl
LOCAL_MODULE_TAGS          := optional
LOCAL_MODULE_CLASS         := JAVA_LIBRARIES
LOCAL_MODULE_SUFFIX        := $(COMMON_JAVA_PACKAGE_SUFFIX)
LOCAL_SRC_FILES            := tcsdkimpl.jar                                                                          
include $(BUILD_PREBUILT)
