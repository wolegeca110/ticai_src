/*!
  @file
  sendcmd.cpp

  @brief
  Places a Remote Procedure Call (RPC) to Android's AtCmdFwd Service

*/

/*===========================================================================

Copyright (c) 2015, Qualcomm Technologies, Inc.
All Rights Reserved.
Confidential and Proprietary - Qualcomm Technologies, Inc.

===========================================================================*/

/*===========================================================================

                        EDIT HISTORY FOR MODULE

This section contains comments describing changes made to the module.
Notice that changes are listed in reverse chronological order.


when       who     what, where, why
--------   ---     ----------------------------------------------------------
04/11/11   jaimel   First cut.


===========================================================================*/


/*===========================================================================

                           INCLUDE FILES

===========================================================================*/

#define LOG_NDEBUG 0
#define LOG_NIDEBUG 0
#define LOG_NDDEBUG 0
#define LOG_TAG "Atfwd_Sendcmd"
#include <utils/Log.h>
#include "common_log.h"
#include <cutils/properties.h>
#include "IAtCmdFwdService.h"
#include <binder/BpBinder.h>
#include <binder/IServiceManager.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <time.h>
#include "sendcmd.h"
#include <fcntl.h>
#define MAX_KEYS 57

#include <binder/IPCThreadState.h>
#include <binder/ProcessState.h>

namespace android {

/*===========================================================================

                           Global Variables

===========================================================================*/

sp<IAtCmdFwdService> gAtCmdFwdService; //At Command forwarding sevice object
sp<DeathNotifier> mDeathNotifier;

/*===========================================================================

                          Extern functions invoked from CKPD daemon

===========================================================================*/

extern "C" int initializeAtFwdService();
extern "C" int pressit(char key, int keyPressTime, int timeBetweenKeyPresses);
extern "C" void millisecondSleep(int milliseconds);

/*===========================================================================
  FUNCTION  initializeAtFwdService
===========================================================================*/
/*!
@brief
     Initializes the connection with the Window Manager service
@return
  Returns 0 if service intialization was successful; -1 otherwise

@note
  None.
*/
/*=========================================================================*/

extern "C" int initializeAtFwdService()
{
    sp<IServiceManager> sm = defaultServiceManager();
    sp<IBinder> binder;
    int retryCnt = 1;
    if(sm == 0) {
        LOGE("Could not obtain IServiceManager \n");
        return -1;
    }

    do {
        binder = sm->getService(String16("AtCmdFwd"));
        if (binder == 0) {
            LOGW("AtCmdFwd service not published, waiting... retryCnt : %d", retryCnt);
            /*
             * Retry after (retryCnt * 5)s and yield in the cases when AtCmdFwd service is
             * is about to be published
             */
            sleep(retryCnt * ATFWD_RETRY_DELAY);
            ++retryCnt;
            continue;
        }

        break;
    } while(retryCnt <= ATFWD_MAX_RETRY_ATTEMPTS);

    if (binder == 0) {
        LOGI("AtCmdFwd service not ready - Exhausted retry attempts - :%d",retryCnt);
        //property_set("ctl.stop", "atfwd");
        return -1;
    }
    if (mDeathNotifier == NULL) {
        mDeathNotifier = new DeathNotifier();
    }
    binder->linkToDeath(mDeathNotifier);

    gAtCmdFwdService = interface_cast<IAtCmdFwdService>(binder);
    if (gAtCmdFwdService == 0)
    {
        LOGE("Could not obtain AtCmdFwd service\n");
        return -1;
    }

    // Start a Binder thread pool to receive Death notification callbacks
    sp<ProcessState> proc(ProcessState::self());
    ProcessState::self()->startThreadPool();
    return 0;
}


/*
*
* add by Ben, 2017.05.24
* return the FCT test results for AT Command(AT+QFCT?)
* 
*/
#define QUECTEL_FCT_TEST  
#ifdef QUECTEL_FCT_TEST

#define ARRARY_SIZE(a) (sizeof(a)/sizeof(a[0]))
#define RESP_BUF_SIZE (380*2) // RESP_BUF_SIZE msut less than the QMI_ATCOP_AT_RESP_MAX_LEN in the file vendor/qcom/proprietary/qmi/inc/qmi_atcop_srvc.h,
			  // Maybe you should change QMI_ATCOP_AT_RESP_MAX_LEN for your requirement

//#define QUECTEL_FCT_TEST_DEBUG // for print debug log info
#ifdef QUECTEL_FCT_TEST_DEBUG
#define DGprintf(fmt,args...) printf(fmt,##args) 
#else
#define DGprintf(fmt, args...) 
#endif 
typedef struct{
	const char *name; // item name
	int result; // -1: fail  0: none  1: success
}fct_item_type;
fct_item_type fct_items_all[]=
//fct_item_type fct_items[]=
{
	{"SECOND LCD", 0},
        {"FLASHLIGHT",0},
	{"KEY", 0},
        {"VIBRATOR",0},
	{"HANDSET PLAY", 0},
	{"HEADSET LOOPBACK", 0},
	{"SPEAKER LOOPBACK", 0},
	{"CAMERA BACK MAIN", 0},
        {"CAMERA BACK AUX",0},
	{"CAMERA FRONT",0},
        {"LIGHT SENSOR",0},
	{"SDCARD", 0},
	{"STORAGE", 0},
	{"WIFI", 0},
	{"BLUETOOTH", 0},
};

extern "C" char* set_response_buf(fct_item_type *fct_items, int num)
{
	int i,offset=0;
	int test_num=0, success_num=0;
	char *resp_buf=NULL;
	if(NULL == resp_buf)
	{
		resp_buf = (char *)malloc(RESP_BUF_SIZE);
		if(resp_buf == NULL)
		{
			LOGI("%s:%d No Memory\n", __func__, __LINE__);
			return resp_buf; // error
		}
		memset(resp_buf, 0, RESP_BUF_SIZE);
	}
	for(i=0; i<num; i++)
	{
		if(fct_items[i].result !=0 )
			test_num ++;
		if(fct_items[i].result == 1)
			success_num ++;
	}
	offset += snprintf(resp_buf+offset, (RESP_BUF_SIZE-offset), "+QFCT: %d,%d,%d",(success_num==num)?1:0, num, test_num );
	if((success_num==num) || (test_num == 0))
	{
		return resp_buf; // all fct items pass or not test
	}
	offset += snprintf(resp_buf+offset, (RESP_BUF_SIZE-offset), "\r\n");
	for(i=0; i<num; i++)
	{
		if(offset+32>RESP_BUF_SIZE)
		{
			printf("There is no space to store results. offset=%d RESP_BUF_SIZE=%d\r\n", offset, RESP_BUF_SIZE);
			break;
		}
		offset += snprintf(resp_buf+offset, (RESP_BUF_SIZE-offset), "+QFCT: %s,", fct_items[i].name);
		DGprintf("[%s] %d\n", fct_items[i].name, fct_items[i].result);
		switch(fct_items[i].result)
		{
			case -1:
				offset += snprintf(resp_buf+offset, (RESP_BUF_SIZE-offset), "fail");
				break;
			case 0:
				offset += snprintf(resp_buf+offset, (RESP_BUF_SIZE-offset), "null");
				break;
				
			case 1:
				offset += snprintf(resp_buf+offset, (RESP_BUF_SIZE-offset), "pass");
				break;
		}
		if(i<num-1)
		{
			offset += snprintf(resp_buf+offset, (RESP_BUF_SIZE-offset), "\r\n");
		}
		
	}
	LOGI("%s:%d: RESP_BUF_SIZE:%d offset:%d \n", __FILE__, __LINE__,RESP_BUF_SIZE, offset);
	printf("%s:%d: RESP_BUF_SIZE:%d offset:%d \n", __FILE__, __LINE__,RESP_BUF_SIZE, offset);
#if  1// for debug
	DGprintf("<<<<<<<<<<<< respbuf >>>>>>>>>>>>>>\n");
	DGprintf("%s", resp_buf);
	DGprintf("<<<<<<<<<<<< respbuf end >>>>>>>>>>>>>>\n");
#endif

	return resp_buf;
}
extern "C" char* get_string_from_two_char(const char *src, char *dest, int size, char start, char end)
{
	char *p=NULL;
	char *q=NULL;
	int i=0;
	if(NULL==src || NULL==dest)
		return NULL;
	memset(dest, 0, size);
	if((p=strchr(src,start)) && (q=strchr(src,end)))
	{
		p++; // skip start char, from next char
		for(i=0;i<size&&p!=q;i++)
		{
			*dest++=*p;
			p++;
			
		}
	}
	else
	{
		return NULL;
	}
	return dest;
	
}

int read_file(const char *filepath, char *buf, int size){
    int fd, len;

    fd = open(filepath, O_RDONLY);
    if(fd == -1){
        printf("[%s]:file(%s) open fail, error=%s\n", __FUNCTION__, filepath, strerror(errno));
        return -1;
    }

    len = read(fd, buf, size - 1);
    if(len > 0){
        if(buf[len - 1] == '\n')
            buf[len - 1] = '\0';
        else
            buf[len] = '\0';
    }

    close(fd);
    return 0;
}

bool is_msm8917(void){
    bool is_msm8917_support = false;
    char soc_name[32];
    int soc_id = 0;
    memset(soc_name, 0x00, sizeof(soc_name));

    if(!read_file("/sys/devices/soc0/soc_id", soc_name, sizeof(soc_name))) {
        soc_id = atoi(soc_name);
    }   
    printf("[%s]:Current Platform Target Soc_id is %d", __FUNCTION__, soc_id);
    if(soc_id == 303){//303 is msm8917,294 is msm8937
        printf("[%s]:Current Platform Target is MSM8917", __FUNCTION__);
        is_msm8917_support = true;
    }else{
        printf("[%s]:Current Platform Target is MSM8937", __FUNCTION__);
        is_msm8917_support = false;
    }

    return is_msm8917_support;
}

extern "C" void quec_qfct_handle(AtCmdResponse *response)
{
	#define FCT_RESULT_FILE	 "/cache/FTM_AP/mmi.res"
	//char *fct_items[] = {"FLASHLIGHT","KEY","HEADSET","VIBRATOR","AUDIO_LOUDSPEAKER","CAMERA_BACK","CAMERA_FRONT","LSENSOR","SDCARD","SIMCARD1","WIFI"};
	int total_items;
	//char *resp_buf = NULL;
	char line_text[64] = {0};
	FILE *fp = NULL;
	//char *ptr = NULL;
	char mmi_res_name[32] = {0};
	int  i,offset=-1;
	//int test_items_num = 0;
        int items_length = ARRARY_SIZE(fct_items_all);
	
        if(is_msm8917()) {
           total_items = items_length -1; 
        } else {
           total_items = items_length;
        }
        fct_item_type fct_items[total_items];        
        if(is_msm8917()) { 
            for(i=0; i<total_items; i++) {
                fct_items[i].name = fct_items_all[i+1].name;
                fct_items[i].result = 0;
	    }
        } else {
            for(i=0; i<total_items; i++) {
                fct_items[i].name = fct_items_all[i].name;
                fct_items[i].result = 0;
	    }
        }
	if((fp=fopen(FCT_RESULT_FILE, "r")) == NULL)
	{
		printf("open file:%s failed!\n", FCT_RESULT_FILE);
		
		if((response->response = set_response_buf(fct_items, total_items)) == NULL )
		{
			response->result = 0; // error
			LOGI("%s:%d open file %s failed!\n", __func__, __LINE__, FCT_RESULT_FILE);
			//printf("%s:%d open file %s failed!\n", __func__, __LINE__, FCT_RESULT_FILE);
		}
		response->result = 1;
		return;
	}
	// get line from file
	while(fgets(line_text, 64, fp)!=NULL)	
	{
	DGprintf("Line: %s \n",line_text);
		if(strchr(line_text, '[') && strchr(line_text, ']')) // [name]
		{
			offset = -1;
			if(get_string_from_two_char(line_text, mmi_res_name, 32, '[',']') == NULL)
			{
				printf("error!\n");
				continue;
			}
			else
			{
				printf("get name:%s\n", mmi_res_name);
			}
			for(i=0; i<total_items; i++)
			{
				DGprintf("fct_item.name:%s\n",fct_items[i].name);
				//if(strstr(line_text, fct_items[i].name))
				if(strcasecmp(mmi_res_name, fct_items[i].name)==0)
				{
					DGprintf("Match\n");
					offset = i;
					break;
				}
				else
				{
					DGprintf("Not Match.\n");
				}
			}
			
		}
		else if(strstr(line_text, "Result"))  // item result
		{
			if(offset>=0)
			{
				if(strstr(line_text, "pass"))
					fct_items[offset].result = 1;
				else if(strstr(line_text, "fail"))
					fct_items[offset].result = -1;
			}
		}
	}
	if(fp)
		fclose(fp);
	
	if((response->response = set_response_buf(fct_items, ARRARY_SIZE(fct_items))) == NULL)
	{
		response->result = 0;
		return;
	}
	response->result = 1; // success
	return;
	
}

#endif

/*===========================================================================
  FUNCTION  sendit
===========================================================================*/
/*!
@brief
     Invokes a Remote Procedure Call (RPC) to Android's Window Manager Service
     Window Manager service returns 0 if the call is successful
@return
  Returns 1 if the key press operation was successful; 0 otherwise

@note
  None.
*/
/*=========================================================================*/

extern "C" AtCmdResponse *sendit(const AtCmd *cmd)
{
    AtCmdResponse *result;

    if (!cmd) return NULL;
#ifdef QUECTEL_FCT_TEST
	result = new AtCmdResponse;
	if(strcasecmp(cmd->name, "+QFCT")==0)
	{
		quec_qfct_handle(result);
	}
#else
    result = gAtCmdFwdService->processCommand(*cmd);
#endif

    return result;
}

void DeathNotifier::binderDied(const wp<IBinder>& who) {
    QCRIL_NOTUSED(who);
    LOGI("AtCmdFwd : binderDied");
    initializeAtFwdService();
}

};  /* namespace android */
