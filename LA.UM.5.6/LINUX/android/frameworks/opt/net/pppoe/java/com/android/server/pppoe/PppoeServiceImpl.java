/*
 * Copyright (C) 2014 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.android.server.pppoe;

import android.content.Context;
import android.content.pm.PackageManager;
import android.net.IEthernetManager;
import android.net.IEthernetServiceListener;
import android.net.IpConfiguration;
import android.net.IpConfiguration.IpAssignment;
import android.net.IpConfiguration.ProxySettings;
import android.os.Binder;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.RemoteCallbackList;
import android.os.RemoteException;
import android.os.SystemProperties;
import android.os.Looper;
import android.provider.Settings;
import android.util.Log;
import android.util.PrintWriterPrinter;

import com.android.internal.util.IndentingPrintWriter;

import java.io.FileDescriptor;
import java.io.PrintWriter;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * EthernetServiceImpl handles remote Ethernet operation requests by implementing
 * the IEthernetManager interface.
 *
 * @hide
 */
public class PppoeServiceImpl{
    private static final String TAG = "PppoeServiceImpl";

    private final Context mContext;
    private final AtomicBoolean mStarted = new AtomicBoolean(false);
    private IpConfiguration mIpConfiguration;

    private Handler mHandler;
    private final PppoeNetworkFactory mTracker;
    //private final RemoteCallbackList<IEthernetServiceListener> mListeners =
    //        new RemoteCallbackList<IEthernetServiceListener>();

    public PppoeServiceImpl(Context context) {
        mContext = context;
        Log.i(TAG, "Creating EthernetConfigStore");

        mTracker = new PppoeNetworkFactory(mContext);
    }

    private void enforceAccessPermission() {
        mContext.enforceCallingOrSelfPermission(
                android.Manifest.permission.ACCESS_NETWORK_STATE,
                "EthernetService");
    }

    private void enforceConnectivityInternalPermission() {
        mContext.enforceCallingOrSelfPermission(
                android.Manifest.permission.CONNECTIVITY_INTERNAL,
                "ConnectivityService");
    }

    public void start() {
        Log.i(TAG, "Starting Ethernet service");

        HandlerThread handlerThread = new HandlerThread("PppoeServiceThread");
        handlerThread.start();
        mHandler = new Handler(handlerThread.getLooper());
		/* BEGIN: Modified by yixing, 2017/12/20   PN:Ethernet customization */
		if(!isDisableEth() && isOpenedEth()){
        	mTracker.start(mContext, mHandler);

        	mStarted.set(true);
		}
		/* END:   Modified by yixing, 2017/12/20 */
    }
	/* BEGIN: Added by yixing, 2017/12/20   PN:Ethernet customization */
	private boolean isDisableEth(){
		return SystemProperties.getBoolean("persist.zt.disable.eth", false);
	}
	private boolean isOpenedEth(){
		return SystemProperties.getBoolean("persist.zt.eth.status", false);
	}
	
	/**
     * add by yixing
     *
     * @hide
     */
	public void Trackstart() { 
		if(isDisableEth()){
			Log.e(TAG, "Ethernet is disable");  
			return;
		}
	    Log.i(TAG, "Start Ethernet Track");  
	    Thread tstopthread = new Thread(new Runnable() {  
	        public void run() {  
	            Looper.prepare();  
	            mTracker.start(mContext, mHandler);
        		mStarted.set(true);  
				SystemProperties.set("persist.zt.eth.status", "true");
	            Looper.loop();  
	        }  
	    });  
	    tstopthread.start();   
	}  
  	/**
     * add by yixing
     *
     * @hide
     */
	public void Trackstop() {  
		if(isDisableEth()){
			Log.e(TAG, "Ethernet is disable");  
			return;
		}
	    Log.i(TAG, "Stop Ethernet Track");  
	    Thread tstopthread = new Thread(new Runnable() {  
	        public void run() {  
	            Looper.prepare();  
	            mTracker.stop();  
	            mStarted.set(false);  
				SystemProperties.set("persist.zt.eth.status", "false");
	            Looper.loop();  
	        }  
	    });  
	    tstopthread.start();  
	} 
	/* END:   Added by yixing, 2017/12/20   PN:Ethernet customization */

    /**
     * Indicates whether the system currently has one or more
     * Ethernet interfaces.
     */
    public boolean isAvailable() {
        enforceAccessPermission();
        return mTracker.isTrackingInterface();
    }
}
